from scapy.all import *
import os
import sys
import datetime

pcapPath = sys.argv[1]
if not os.path.exists(pcapPath):
    exit("The path to pcap is not correct!")

packets = PcapReader(pcapPath)

class Flow:
    def __init__(self, packet, packetInfo):
        self.flowPackets = [packet]
        self.allFlowPacketInfos = [packetInfo]
        self.packetInfo = packetInfo

    def addPacketToFlow(self, packet, packetInfo):
        self.flowPackets.append(packet)
        self.allFlowPacketInfos.append(packetInfo)
        

allFlows = []
start = 0

def findFlow(packetInfo, packet):
    flowIsAdded = False
    global start
    for f in range(start, len(allFlows)):
        flow = allFlows[f]
        if packetInfo["packetProtocol"] == flow.packetInfo["packetProtocol"]:
            if ((packetInfo["packetSrc"] == flow.packetInfo["packetSrc"] and\
            packetInfo["packetDst"] == flow.packetInfo["packetDst"] and\
            packetInfo["srcPort"] == flow.packetInfo["srcPort"] and\
            packetInfo["dstPort"] == flow.packetInfo["dstPort"]) or\
            (packetInfo["packetSrc"] == flow.packetInfo["packetDst"] and\
            packetInfo["packetDst"] == flow.packetInfo["packetSrc"] and\
            packetInfo["srcPort"] == flow.packetInfo["dstPort"] and\
            packetInfo["dstPort"] == flow.packetInfo["srcPort"])):
                if packetInfo["packetTime"] - flow.packetInfo["packetTime"] < 15 and not flowIsAdded:
                    flow.packetInfo["packetTime"] = packetInfo["packetTime"]
                    flow.addPacketToFlow(packet, packetInfo)
                    flowIsAdded = True
                    break
                elif not flowIsAdded:
                    newFlow = Flow(packet, packetInfo)
                    allFlows.append(newFlow)
                    flowIsAdded = True
                    start += 1
                    break
    if not flowIsAdded:
        newFlow = Flow(packet, packetInfo)
        allFlows.append(newFlow)
        flowIsAdded = True
    return

for packet in packets:
    if packet.haslayer(IP) and (packet.haslayer(TCP) or packet.haslayer(UDP)):
        packetTime = packet.time
        packetSrc = packet[IP].src
        packetDst = packet[IP].dst
        srcPort = packet[IP].sport
        dstPort = packet[IP].dport
        if packet.haslayer(TCP):
            packetProtocol = "TCP"
        else:
            packetProtocol = "UDP"
        packetInfo = {"packetTime": packetTime,
                      "packetSrc": packetSrc, 
                      "packetDst": packetDst, 
                      "srcPort": srcPort, 
                      "dstPort": dstPort, 
                      "packetProtocol": packetProtocol}
        findFlow(packetInfo, packet)
        
for f in allFlows:
    ans = str(f.packetInfo["packetSrc"]) + ',' + str(f.packetInfo["packetDst"]) + ',' +\
        str(f.packetInfo["srcPort"]) + ',' + str(f.packetInfo["dstPort"]) + ',' +\
        f.packetInfo["packetProtocol"]
    print(ans)
