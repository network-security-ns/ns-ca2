from scapy.all import *
import os
import sys
import datetime

NO_TLS = 0
TLS_PHASE_CLIENT_HELLO = 1
TLS_PHASE_SERVER_HELLO = 2
TLS_PHASE_CLIENT_KEY_EXCHANGE = 4
TLS_PHASE_SERVER_NEW_SESSION_TICKET = 5

HANDSHAKE = 22
TLS_CLIENT_HELLO = 1
TLS_SERVER_HELLO = 2
TLS_CLIENT_KEY_EXCHANGE = 16
TLS_SERVER_NEW_SESSION_TICKET = 4

EXTENSION_TYPE_SERVER_NAME = 0

pcapPath = sys.argv[1]
if not os.path.exists(pcapPath):
    exit("The path to pcap is not correct!")

packets = PcapReader(pcapPath)

class Flow:
    def __init__(self, packet, packetInfo):
        self.flowPackets = [packet]
        self.allFlowPacketInfos = [packetInfo]
        self.packetInfo = packetInfo

    def addPacketToFlow(self, packet, packetInfo):
        self.flowPackets.append(packet)
        self.allFlowPacketInfos.append(packetInfo)        

allFlows = []
start = 0

def findFlow(packetInfo, packet):
    flowIsAdded = False
    global start
    for f in range(start, len(allFlows)):
        flow = allFlows[f]
        if packetInfo["packetProtocol"] == flow.packetInfo["packetProtocol"]:
            if ((packetInfo["packetSrc"] == flow.packetInfo["packetSrc"] and\
            packetInfo["packetDst"] == flow.packetInfo["packetDst"] and\
            packetInfo["srcPort"] == flow.packetInfo["srcPort"] and\
            packetInfo["dstPort"] == flow.packetInfo["dstPort"]) or\
            (packetInfo["packetSrc"] == flow.packetInfo["packetDst"] and\
            packetInfo["packetDst"] == flow.packetInfo["packetSrc"] and\
            packetInfo["srcPort"] == flow.packetInfo["dstPort"] and\
            packetInfo["dstPort"] == flow.packetInfo["srcPort"])):
                if packetInfo["packetTime"] - flow.packetInfo["packetTime"] < 15 and not flowIsAdded:
                    flow.packetInfo["packetTime"] = packetInfo["packetTime"]
                    flow.addPacketToFlow(packet, packetInfo)
                    flowIsAdded = True
                    break
                elif not flowIsAdded:
                    newFlow = Flow(packet, packetInfo)
                    allFlows.append(newFlow)
                    flowIsAdded = True
                    start += 1
                    break
    if not flowIsAdded:
        newFlow = Flow(packet, packetInfo)
        allFlows.append(newFlow)
        flowIsAdded = True
    return 


def setTlsPhaseOther(packet):
    tlsPhase = NO_TLS
    if packet.haslayer(TLS):
        if packet[TLS].type == HANDSHAKE:
            if packet[TLS].msg[0].msgtype == TLS_CLIENT_HELLO:
                tlsPhase = TLS_PHASE_CLIENT_HELLO
            elif packet[TLS].msg[0].msgtype == TLS_SERVER_HELLO:
                tlsPhase = TLS_PHASE_SERVER_HELLO
            elif packet[TLS].msg[0].msgtype == TLS_CLIENT_KEY_EXCHANGE:
                tlsPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
            elif packet[TLS].msg[0].msgtype == TLS_SERVER_NEW_SESSION_TICKET:
                tlsPhase = TLS_PHASE_SERVER_NEW_SESSION_TICKET
    return tlsPhase


def setTlsPhase(packet):
    tlsPhase = NO_TLS
    pkt = bytes_hex(packet).decode("utf-8")
    if len(pkt) > 108:
        tlsContentType = int(pkt[108:110], 16)
        tlsHandshakeType = int(pkt[118:120], 16)
        if tlsContentType == HANDSHAKE:
            if tlsHandshakeType == TLS_CLIENT_HELLO:
                tlsPhase = TLS_PHASE_CLIENT_HELLO
            elif tlsHandshakeType == TLS_SERVER_HELLO:
                tlsPhase = TLS_PHASE_SERVER_HELLO
            elif tlsHandshakeType == TLS_CLIENT_KEY_EXCHANGE:
                tlsPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
            elif tlsHandshakeType == TLS_SERVER_NEW_SESSION_TICKET:
                tlsPhase = TLS_PHASE_SERVER_NEW_SESSION_TICKET
    return tlsPhase


def is_https(allFlowPacketInfos):
    currentPhase = NO_TLS
    for pi in allFlowPacketInfos:
        if currentPhase == NO_TLS and pi["tlsPhase"] == TLS_PHASE_CLIENT_HELLO:
            currentPhase = TLS_PHASE_CLIENT_HELLO
        elif currentPhase == TLS_PHASE_CLIENT_HELLO and\
        pi["tlsPhase"] == TLS_PHASE_SERVER_HELLO:
            currentPhase = TLS_PHASE_SERVER_HELLO
        elif currentPhase == TLS_PHASE_SERVER_HELLO and\
        pi["tlsPhase"] == TLS_PHASE_CLIENT_KEY_EXCHANGE:
            currentPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
        elif currentPhase == TLS_PHASE_CLIENT_KEY_EXCHANGE and\
        pi["tlsPhase"] == TLS_PHASE_SERVER_NEW_SESSION_TICKET:
            currentPhase = TLS_PHASE_SERVER_NEW_SESSION_TICKET
            return True
    return False


def get_sni_other(flowPackets, allFlowPacketInfos):
    for p in range(len(flowPackets)):
        if allFlowPacketInfos[p]["tlsPhase"] == TLS_PHASE_CLIENT_HELLO:
            return flowPackets[p][TLS].msg[0]["TLS Extension - Server Name"].servernames[0].servername.decode()
    return "The given flow is not https"


def get_sni(flowPackets, allFlowPacketInfos):
    for p in range(len(flowPackets)):
        if allFlowPacketInfos[p]["tlsPhase"] == TLS_PHASE_CLIENT_HELLO:
            pkt = bytes_hex(flowPackets[p]).decode("utf-8")
            sessionIdLenIndex = 108 + 43 * 2
            sessionIdLen = int(pkt[sessionIdLenIndex: sessionIdLenIndex + 1 * 2], 16)
            cipherSuitesLenIndex = sessionIdLenIndex + 1 * 2 + sessionIdLen * 2
            cipherSuitesLen = int(pkt[cipherSuitesLenIndex: cipherSuitesLenIndex + 2 * 2], 16)
            compressionMethodLenIndex = cipherSuitesLenIndex + 2 * 2 + cipherSuitesLen * 2
            compressionMethodLen = int(pkt[compressionMethodLenIndex: compressionMethodLenIndex + 1 * 2], 16)
            extensionsStartIndex = compressionMethodLenIndex + 1 * 2 + compressionMethodLen * 2 + 2 * 2
            extType = int(pkt[extensionsStartIndex: extensionsStartIndex + 2 * 2], 16)
            extLenIndex = extensionsStartIndex + 2 * 2
            extLen = int(pkt[extLenIndex: extLenIndex + 2 * 2], 16)
            while extType != EXTENSION_TYPE_SERVER_NAME:
                extensionsStartIndex = extLenIndex + 2 * 2 + extLen * 2
                extType = int(pkt[extensionsStartIndex: extensionsStartIndex + 2 * 2], 16)
                extLenIndex = extensionsStartIndex + 2 * 2
                extLen = int(pkt[extLenIndex: extLenIndex + 2 * 2], 16)
            if extType == EXTENSION_TYPE_SERVER_NAME:
                sniLenIndex = extLenIndex + (2 + 2 + 1) * 2
                sniLen = int(pkt[sniLenIndex: sniLenIndex + 2 * 2], 16)
                sniIndex = sniLenIndex + 2 * 2
                sniHex = pkt[sniIndex: sniIndex + sniLen * 2]
                sni = bytearray.fromhex(sniHex).decode()
                return sni
    return "The given flow is not https"


load_layer("tls")
for packet in packets:
    if packet.haslayer(IP) and (packet.haslayer(TCP) or packet.haslayer(UDP)):
        packetTime = packet.time
        packetSrc = packet[IP].src
        packetDst = packet[IP].dst
        srcPort = packet[IP].sport
        dstPort = packet[IP].dport
        tlsPhase = setTlsPhase(packet)
        if packet.haslayer(TCP):
            packetProtocol = "TCP"
        else:
            packetProtocol = "UDP" 
        packetInfo = {"packetTime": packetTime,
                      "packetSrc": packetSrc, 
                      "packetDst": packetDst, 
                      "srcPort": srcPort, 
                      "dstPort": dstPort, 
                      "packetProtocol": packetProtocol,
                      "tlsPhase": tlsPhase}
        findFlow(packetInfo, packet)
        

for f in allFlows:
    isHttps = is_https(f.allFlowPacketInfos)
    if isHttps:
        sni = get_sni(f.flowPackets, f.allFlowPacketInfos)
        ans = str(f.packetInfo["packetSrc"]) + ',' + str(f.packetInfo["packetDst"]) + ',' +\
            str(f.packetInfo["srcPort"]) + ',' + str(f.packetInfo["dstPort"]) + ',' +\
            f.packetInfo["packetProtocol"] + ',' + sni
        print(ans)
