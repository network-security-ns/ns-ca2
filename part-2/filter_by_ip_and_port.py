from netfilterqueue import NetfilterQueue
from scapy.all import *
import os
import sys


def checkPacketIpAndPort(pkt):
    packet = IP(pkt.get_payload())
    if packet.haslayer(TCP) or packet.haslayer(UDP):
        destIP = str(packet[IP].dst)
        destPort = str(packet[IP].dport)
        if (destIP, destPort) in invalidDests:
            print("The destination is invalid! The packet is dropped!")
            pkt.drop()
        else:
            # print("The destination is valid! The packet is accepted!")
            pkt.accept()
    else:
        pkt.accept()


destFilePath = sys.argv[1]
if not os.path.exists(destFilePath):
    exit("The path to destination's IP and port list is not correct!")

destFile = open(destFilePath, 'r')
dests = destFile.read().splitlines()
invalidDests = []

for d in dests:
    ip, port = d.split(',')
    invalidDests.append((ip, port))

nfqueue = NetfilterQueue()
nfqueue.bind(1, checkPacketIpAndPort)
try:
    nfqueue.run()
except KeyboardInterrupt:
    print('')

nfqueue.unbind()
