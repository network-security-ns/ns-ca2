from netfilterqueue import NetfilterQueue


def printAndAccept(pkt):
    print(pkt)
    pkt.accept()

nfqueue = NetfilterQueue()
nfqueue.bind(1, printAndAccept)
try:
    nfqueue.run()
except KeyboardInterrupt:
    print('')

nfqueue.unbind()