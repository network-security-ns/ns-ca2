from netfilterqueue import NetfilterQueue
from scapy.all import *
import os
import sys

NO_TLS = 0
TLS_PHASE_CLIENT_HELLO = 1
TLS_PHASE_SERVER_HELLO = 2
TLS_PHASE_CLIENT_KEY_EXCHANGE = 4
TLS_PHASE_SERVER_NEW_SESSION_TICKET = 5

HANDSHAKE = 22
TLS_CLIENT_HELLO = 1
TLS_SERVER_HELLO = 2
TLS_CLIENT_KEY_EXCHANGE = 16
TLS_CLIENT_CHANGE_CIPHER_SPEC = 20
TLS_SERVER_NEW_SESSION_TICKET = 4

EXTENSION_TYPE_SERVER_NAME = 0


class Flow:
    def __init__(self, packet, packetInfo):
        self.flowPackets = [packet]
        self.allFlowPacketInfos = [packetInfo]
        self.packetInfo = packetInfo
        self.isHttpsFlow = False
        self.sni = ""

    def addPacketToFlow(self, packet, packetInfo):
        self.flowPackets.append(packet)
        self.allFlowPacketInfos.append(packetInfo)        

allFlows = []
start = 0

def findFlow(packetInfo, packet):
    flowIsAdded = False
    global start
    flowIndex = -1
    for f in range(start, len(allFlows)):
        flow = allFlows[f]
        flowIndex = f
        if packetInfo["packetProtocol"] == flow.packetInfo["packetProtocol"]:
            if ((packetInfo["packetSrc"] == flow.packetInfo["packetSrc"] and\
            packetInfo["packetDst"] == flow.packetInfo["packetDst"] and\
            packetInfo["srcPort"] == flow.packetInfo["srcPort"] and\
            packetInfo["dstPort"] == flow.packetInfo["dstPort"]) or\
            (packetInfo["packetSrc"] == flow.packetInfo["packetDst"] and\
            packetInfo["packetDst"] == flow.packetInfo["packetSrc"] and\
            packetInfo["srcPort"] == flow.packetInfo["dstPort"] and\
            packetInfo["dstPort"] == flow.packetInfo["srcPort"])):
                if packetInfo["packetTime"] - flow.packetInfo["packetTime"] < 15 and not flowIsAdded:
                    flow.packetInfo["packetTime"] = packetInfo["packetTime"]
                    flow.addPacketToFlow(packet, packetInfo)
                    flowIsAdded = True
                    return flowIndex
                elif not flowIsAdded:
                    newFlow = Flow(packet, packetInfo)
                    allFlows.append(newFlow)
                    flowIndex = len(allFlows) - 1
                    flowIsAdded = True
                    start += 1
                    return flowIndex
    if not flowIsAdded:
        newFlow = Flow(packet, packetInfo)
        allFlows.append(newFlow)
        flowIndex = len(allFlows) - 1
        flowIsAdded = True
    return flowIndex


def setTlsPhaseOther(packet):
    tlsPhase = NO_TLS
    if packet.haslayer(TLS):
        if packet[TLS].type == HANDSHAKE:
            if packet[TLS].msg[0].msgtype == TLS_CLIENT_HELLO:
                tlsPhase = TLS_PHASE_CLIENT_HELLO
            elif packet[TLS].msg[0].msgtype == TLS_SERVER_HELLO:
                tlsPhase = TLS_PHASE_SERVER_HELLO
            elif packet[TLS].msg[0].msgtype == TLS_CLIENT_KEY_EXCHANGE:     #4
                tlsPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
            elif packet[TLS].msg[0].msgtype == TLS_CLIENT_CHANGE_CIPHER_SPEC:   #4
                tlsPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
            elif packet[TLS].msg[0].msgtype == TLS_SERVER_NEW_SESSION_TICKET:
                tlsPhase = TLS_PHASE_SERVER_NEW_SESSION_TICKET
    return tlsPhase


def setTlsPhase(packet):
    tlsPhase = NO_TLS
    pkt = bytes_hex(packet).decode("utf-8")
    if len(pkt) > 108:
        if packet.haslayer(TLS):
            packet.show()
        tlsContentType = int(pkt[108:110], 16)
        tlsHandshakeType = int(pkt[118:120], 16)
        if tlsContentType == HANDSHAKE:
            if tlsHandshakeType == TLS_CLIENT_HELLO:
                tlsPhase = TLS_PHASE_CLIENT_HELLO
            elif tlsHandshakeType == TLS_SERVER_HELLO:
                tlsPhase = TLS_PHASE_SERVER_HELLO
            elif tlsHandshakeType == TLS_CLIENT_KEY_EXCHANGE:       #4
                tlsPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
            elif tlsHandshakeType == TLS_CLIENT_CHANGE_CIPHER_SPEC:     #4
                tlsPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
            elif tlsHandshakeType == TLS_SERVER_NEW_SESSION_TICKET:
                tlsPhase = TLS_PHASE_SERVER_NEW_SESSION_TICKET
    return tlsPhase


def calcHeaderLens(packet):
    etherHeaderLen = 0
    if packet.haslayer(Ether):
        etherLen = len(packet[Ether])
        etherHeaderLen = etherLen - len(packet[Ether].payload)
    ipLen = len(packet[IP])
    ipHeaderLen = ipLen - len(packet[IP].payload)
    tcpLen = len(packet[TCP])
    tcpHeaderLen = tcpLen - len(packet[TCP].payload) 
    paddingLen = 0
    if packet.haslayer(Padding):
        paddingLen = len(packet[Padding])
    # print(etherHeaderLen)
    # print(ipHeaderLen)
    # print(tcpHeaderLen)
    # print(paddingLen)
    ethIpTcpHeaderTotalLen = etherHeaderLen * 2 + ipHeaderLen * 2 + tcpHeaderLen * 2 + paddingLen * 2
    return ethIpTcpHeaderTotalLen


def setTlsPhaseThisPart(packet):
    tlsPhase = NO_TLS
    pkt = bytes_hex(packet).decode("utf-8")
    ethIpTcpHeaderTotalLen = calcHeaderLens(packet)
    if len(pkt) > ethIpTcpHeaderTotalLen:
        tlsContentTypeIndex = ethIpTcpHeaderTotalLen
        tlsContentType = int(pkt[tlsContentTypeIndex: tlsContentTypeIndex + 2], 16)
        tlsHandshakeTypeIndex = tlsContentTypeIndex + 2 + 2 * 2 + 2 * 2
        tlsHandshakeType = int(pkt[tlsHandshakeTypeIndex: tlsHandshakeTypeIndex + 2], 16)
        if tlsContentType == HANDSHAKE:
            if tlsHandshakeType == TLS_CLIENT_HELLO:
                tlsPhase = TLS_PHASE_CLIENT_HELLO
            elif tlsHandshakeType == TLS_SERVER_HELLO:
                tlsPhase = TLS_PHASE_SERVER_HELLO
            elif tlsHandshakeType == TLS_CLIENT_KEY_EXCHANGE:
                tlsPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
            elif tlsHandshakeType == TLS_SERVER_NEW_SESSION_TICKET:
                tlsPhase = TLS_PHASE_SERVER_NEW_SESSION_TICKET
    # print("tlsPhase: ", tlsPhase)
    return tlsPhase


def is_https(allFlowPacketInfos):
    currentPhase = NO_TLS
    for pi in allFlowPacketInfos:
        if currentPhase == NO_TLS and pi["tlsPhase"] == TLS_PHASE_CLIENT_HELLO:
            currentPhase = TLS_PHASE_CLIENT_HELLO
        elif currentPhase == TLS_PHASE_CLIENT_HELLO and\
        pi["tlsPhase"] == TLS_PHASE_CLIENT_KEY_EXCHANGE:
            currentPhase = TLS_PHASE_CLIENT_KEY_EXCHANGE
            return True
    return False


def get_sni_other(flowPackets, allFlowPacketInfos):
    for p in range(len(flowPackets)):
        if allFlowPacketInfos[p]["tlsPhase"] == TLS_PHASE_CLIENT_HELLO:
            return flowPackets[p][TLS].msg[0]["TLS Extension - Server Name"].servernames[0].servername.decode()
    return "The given flow is not https"


def get_sni(flowPackets, allFlowPacketInfos):
    for p in range(len(flowPackets)):
        if allFlowPacketInfos[p]["tlsPhase"] == TLS_PHASE_CLIENT_HELLO:
            pkt = bytes_hex(flowPackets[p]).decode("utf-8")
            ethIpTcpHeaderTotalLen = calcHeaderLens(flowPackets[p])
            sessionIdLenIndex = ethIpTcpHeaderTotalLen + 43 * 2
            sessionIdLen = int(pkt[sessionIdLenIndex: sessionIdLenIndex + 1 * 2], 16)
            cipherSuitesLenIndex = sessionIdLenIndex + 1 * 2 + sessionIdLen * 2
            cipherSuitesLen = int(pkt[cipherSuitesLenIndex: cipherSuitesLenIndex + 2 * 2], 16)
            compressionMethodLenIndex = cipherSuitesLenIndex + 2 * 2 + cipherSuitesLen * 2
            compressionMethodLen = int(pkt[compressionMethodLenIndex: compressionMethodLenIndex + 1 * 2], 16)
            extensionsStartIndex = compressionMethodLenIndex + 1 * 2 + compressionMethodLen * 2 + 2 * 2
            extType = int(pkt[extensionsStartIndex: extensionsStartIndex + 2 * 2], 16)
            extLenIndex = extensionsStartIndex + 2 * 2
            extLen = int(pkt[extLenIndex: extLenIndex + 2 * 2], 16)
            while extType != EXTENSION_TYPE_SERVER_NAME:
                extensionsStartIndex = extLenIndex + 2 * 2 + extLen * 2
                extType = int(pkt[extensionsStartIndex: extensionsStartIndex + 2 * 2], 16)
                extLenIndex = extensionsStartIndex + 2 * 2
                extLen = int(pkt[extLenIndex: extLenIndex + 2 * 2], 16)
            if extType == EXTENSION_TYPE_SERVER_NAME:
                sniLenIndex = extLenIndex + (2 + 2 + 1) * 2
                sniLen = int(pkt[sniLenIndex: sniLenIndex + 2 * 2], 16)
                sniIndex = sniLenIndex + 2 * 2
                sniHex = pkt[sniIndex: sniIndex + sniLen * 2]
                sni = bytearray.fromhex(sniHex).decode()
                return sni
    return "The given flow is not https"


def checkPacketProtocol(pkt):
    packet = IP(pkt.get_payload())
    if packet.haslayer(IP) and (packet.haslayer(TCP) or packet.haslayer(UDP)):
        packetTime = packet.time
        packetSrc = packet[IP].src
        packetDst = packet[IP].dst
        srcPort = packet[IP].sport
        dstPort = packet[IP].dport
        tlsPhase = NO_TLS
        if packet.haslayer(TCP):
            packetProtocol = "TCP"
            tlsPhase = setTlsPhaseThisPart(packet)
        else:
            packetProtocol = "UDP" 
        packetInfo = {"packetTime": packetTime,
                      "packetSrc": packetSrc, 
                      "packetDst": packetDst, 
                      "srcPort": srcPort, 
                      "dstPort": dstPort, 
                      "packetProtocol": packetProtocol,
                      "tlsPhase": tlsPhase}
        flowIndex = findFlow(packetInfo, packet)
        
        packetHex = bytes_hex(packet).decode("utf-8")
        
        if packet.haslayer(UDP) and dstPort == 53:      # DNS
            domainName = (packet["DNS Question Record"].qname).decode("utf-8")
            if domainName[-1] == '.':
                    domainName = domainName[:-1]
            if domainName in blacklist:
                print("The protocol is DNS & the domain is in blacklist! The packet is dropped!")
                pkt.drop()
                return
            else:
                pkt.accept()
                return
        elif "48545450" in packetHex:       # HTTP
            if "474554" in packetHex or "504f5354" in packetHex or \
              "505554" in packetHex or "44454c455445" in packetHex or \
              "48454144" in packetHex or "434f4e4e454354" in packetHex or \
              "4f5054494f4e53" in packetHex or "5452414345" in packetHex or \
              "5041544348" in packetHex:
                hostName = (packet["HTTP Request"].Host).decode("utf-8")
                if hostName in blacklist:
                    print("The protocol is HTTP & the domain is in blacklist! The packet is dropped!")
                    pkt.drop()
                    return
                else:
                    pkt.accept()
                    return
        else:
            currentFlow = allFlows[flowIndex]       # HTTPS
            if not currentFlow.isHttpsFlow:
                isHttps = is_https(currentFlow.allFlowPacketInfos)
                currentFlow.isHttpsFlow = isHttps
            if currentFlow.isHttpsFlow:
                sni = get_sni(currentFlow.flowPackets, currentFlow.allFlowPacketInfos)
                currentFlow.sni = sni
                if currentFlow.sni in blacklist:
                    print("The protocol is HTTPS & the domain is in blacklist! The packet is dropped!")
                    pkt.drop()
                    return
                else:
                    pkt.accept()
                    return
            else:
                pkt.accept()
                return  
    else:
        pkt.accept()
        return


blacklistFilePath = sys.argv[1]
if not os.path.exists(blacklistFilePath):
    exit("The path to blacklist is not correct!")

blacklistFile = open(blacklistFilePath, 'r')
blacklist = blacklistFile.read().splitlines()

load_layer("tls")
load_layer("http")
nfqueue = NetfilterQueue()
nfqueue.bind(1, checkPacketProtocol)
try:
    nfqueue.run()
except KeyboardInterrupt:
    print('')

nfqueue.unbind()
