Windows:
python extract_network_flows.py "D:\Uni\Python\NS\CA2\Computer Assignment 1\pcaps\sara-10.0.2.112.pcap"
python extract_network_https_flows.py "D:\Uni\Python\NS\CA2\Computer Assignment 1\pcaps\sara-10.0.2.112.pcap"
python extract_network_sni_flows.py "D:\Uni\Python\NS\CA2\Computer Assignment 1\pcaps\sara-10.0.2.112.pcap"


Linux:
python3 extract_network_flows.py "../Computer Assignment 1/pcaps/sara-10.0.2.112.pcap"
python3 extract_network_https_flows.py "../Computer Assignment 1/pcaps/sara-10.0.2.112.pcap"
python3 extract_network_sni_flows.py "../Computer Assignment 1/pcaps/sara-10.0.2.112.pcap"
sudo python3 print_output_packets.py
# iptables help: #####################################################
sudo iptables -I OUTPUT -j NFQUEUE --queue-num 1    (insert rule)
sudo iptables -D OUTPUT -j NFQUEUE --queue-num 1    (delete rule)
sudo iptables -L OUTPUT
sudo iptables -S
sudo iptables -L --line-numbers
######################################################################
sudo python3 filter_by_ip_and_port.py invalid_dests.txt
# nping: #############################################################
sudo nping --tcp -p 80 185.188.105.10
sudo nping --tcp -p 80 172.217.17.110
######################################################################
sudo python3 filter_by_protocol.py
# nping: #############################################################
sudo nping --tcp -p 80 scanme.nmap.org
######################################################################
sudo python3 filter_by_domain.py blacklist.txt
# nping: #############################################################
sudo nping --tcp -p 80 ut.ac.ir
######################################################################
